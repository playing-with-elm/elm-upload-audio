port module Ports exposing (..)


type alias AudioPortData =
    { content : String
    , filename : String
    , type_ : String
    }


port fileSelected : String -> Cmd msg


port fileContentRead : (AudioPortData -> msg) -> Sub msg
