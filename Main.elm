module Main exposing (..)

import Html exposing (Html, Attribute, text, program, audio, input, div, button)
import Html.Attributes exposing (src, title, class, id, type_, controls, placeholder)
import Html.Events exposing (on, onClick, onInput)
import Json.Decode as Json
import Ports exposing (AudioPortData, fileSelected, fileContentRead)
import Debug


type Msg
    = NoOp
    | OnFileSelected
    | NewMediaUrl String
    | AudioRead AudioPortData


type alias Model =
    { id : String
    , mAudio : Maybe Audio
    }


type alias Audio =
    { content : String
    , filename : String
    , type_ : String
    }


main : Program Never Model Msg
main =
    program
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


init : ( Model, Cmd Msg )
init =
    ( { id = "AudioInputId"
      , mAudio = Nothing
      }
    , Cmd.none
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        OnFileSelected ->
            ( model
            , fileSelected model.id
            )

        NewMediaUrl newUrl ->
            let
                newAudio =
                    { content = newUrl
                    , filename = "filename"
                    , type_ = "type_"
                    }
            in
                ( { model | mAudio = Just newAudio }
                , Cmd.none
                )

        AudioRead data ->
            let
                newAudio =
                    { content = data.content
                    , filename = data.filename
                    , type_ = data.type_
                    }
            in
                ( { model | mAudio = Just newAudio }
                , Cmd.none
                )

        NoOp ->
            ( model, Cmd.none )


view : Model -> Html Msg
view model =
    let
        audioPlayer =
            case model.mAudio of
                Just newAudio ->
                    viewAudio newAudio

                Nothing ->
                    viewAudio defaultAudio
    in
        div []
            [ div [] [ audioPlayer ]
            , div []
                [ input
                    [ type_ "file"
                    , id model.id
                    , on "change" (Json.succeed OnFileSelected)
                    ]
                    []
                , div []
                    [ input
                        [ type_ "url"
                        , placeholder "from URL"
                        , onInput NewMediaUrl
                        ]
                        []
                    ]
                ]
            ]


viewAudio : Audio -> Html Msg
viewAudio a =
    audio
        [ id "audio"
        , src a.content
        , title a.filename
        , type_ a.type_
        , controls True
        ]
        []


defaultAudio =
    { content = ""
    , filename = ""
    , type_ = ""
    }


subscriptions : Model -> Sub Msg
subscriptions model =
    fileContentRead AudioRead
